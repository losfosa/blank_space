extends Node2D

var sound = SingletonSound.make_player_sound_fx(SingletonSound.LEVEL_FINISHED, -12)

func _init():
	add_child(sound)
	sound.play()


func _ready():
	$CanvasLayer/Label.text = tr("complete_level")
	$AnimationPlayer.play("Appear")


func _on_EndGame_body_entered(body:Node):
	var bee = GlobalVariables.get_player()
	if body == bee:
		bee.limit_space(0, 10000000)
		Game_Events.emit_signal("level_finished")
