extends CanvasLayer

var music_player


func _init():
	var menu_music = load("res://Assets/audio/in_game_music/menus/game_won.ogg")
	music_player = SingletonSound.make_player_sound_fx(menu_music) as Sound


func _ready():
	add_child(music_player)
	music_player.play()
	determinate_language()


func _on_Start_button_down():
	GlobalVariables.game_stage_level = 0
	music_player.stop()
	GlobalVariables.scene_switcher.change_scene("res://Game/GameCore.tscn")


func _on_Exit_button_down():
	GameControl.quit_game()


func determinate_language():
	$Label.text = tr("won_title")
	$Start/Label.text = tr("won_start")
	$Exit/Label.text = tr("won_exit")
