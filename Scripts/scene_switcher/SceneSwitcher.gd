extends Node

var latest_scene: Node

func _init():
	GlobalVariables.scene_switcher = self

func _ready():
	change_scene("res://Scenes/Menu/Menu.tscn")


func change_scene(new_scene_path: String):
	if latest_scene != null:
		latest_scene.queue_free()

	latest_scene = load(new_scene_path).instance()
	add_child(latest_scene)


func change_scene_using_node(node : Node):
	if latest_scene != null:
		latest_scene.queue_free()
	
	latest_scene = node
	add_child(latest_scene)
