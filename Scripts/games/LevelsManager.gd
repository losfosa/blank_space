extends Node2D
class_name LevelsManager

var levels = [
	load("res://Scripts/games/Level_1.gd"),
	load("res://Scripts/games/Level_2.gd"),
	load("res://Scripts/games/Level_3.gd"),
	load("res://Scripts/games/Level_4.gd")
]

var current_level: Node2D
var game_stage_level
var screen_transitioner


func _ready():
	# warning-ignore:return_value_discarded
	Game_Events.connect("level_finished", self, "on_level_finished")

	screen_transitioner = load("res://Scenes/ScreenTransitioner.tscn").instance()
	screen_transitioner.connect("transitioned", self, "on_screen_transitioned")
	GlobalVariables.user_interface.add_child_first(screen_transitioner)
	game_stage_level = GlobalVariables.game_stage_level
	start_level()


func _process(_delta):
	if not GlobalVariables.get_player().is_alive():
		# warning-ignore:return_value_discarded
		GlobalVariables.scene_switcher.change_scene("res://Scenes/game_over/GameOver.tscn")


func on_level_finished():
	game_stage_level += 1
	if game_stage_level < levels.size():
		GlobalVariables.game_stage_level = game_stage_level
		current_level.queue_free()
		start_level()
	else:
		GlobalVariables.scene_switcher.change_scene("res://Scenes/game_won/Game_Won.tscn")


func init_popup(message: String):
	var PopupGoal = load("res://UI/popup/level_goal.tscn")
	var popup = PopupGoal.instance()
	add_child(popup)
	popup.start(message)


func on_screen_transitioned():
	current_level.start()


func create_level():
	current_level = Node2D.new()
	current_level.name = "Level"
	current_level.set_script(levels[game_stage_level])
	add_child(current_level)


func start_level():
	create_level()
	init_popup(current_level.get_message())
	show_transition()


func show_transition():
	screen_transitioner.transition(current_level.level_bg)
