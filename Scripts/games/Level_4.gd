extends "res://Scripts/games/Level.gd"
var alert_waves = preload("res://UI/hazard_alert/hazard_alert.tscn").instance()

var Towers = preload("res://UI/towers/CommTowers.tscn")
var towers: CommTowers
var TOWERS_X_POS = 1000
var stress_button = load("res://UI/stress_button/stress_button.tscn").instance()

var waves_generator


func _init():
	._init()
	level_bg = "urban_city"
	level_condition = tr("condition_level4")


func _ready():
	waves_generator = level_resources.get_waves_generator()
	add_child(waves_generator)
	
	alert_waves.show_alert()
	add_child(alert_waves)
	stress_button.set_lvl_bool(false)
	stress_button._ready()
	add_child(stress_button)

	pollen_score_view = level_resources.get_pollen_score()
	check_player_score_manager()
	stress_bar_view = level_resources.get_stress_bar()
	check_player_stress_bar_manager()

	init_towers(TOWERS_X_POS)
	waves_generator.set_cooldown(5)
	waves_generator.set_can_repeat(false)


func start():
	waves_generator.start()
	.start()


func init_towers(default_position):
	towers = Towers.instance()
	call_deferred("add_child", towers)
	towers.set_position(default_position)
	towers.set_generator(waves_generator)
	# warning-ignore:return_value_discarded
	towers.connect("finished", self, "finish")


func finish():
	waves_generator.stop()
	.remove_node(stress_bar_view)
	.remove_node(pollen_score_view)
	.remove_node(stress_button)
	.finish()
