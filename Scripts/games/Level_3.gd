extends "res://Scripts/games/Level.gd"
var alert_waves = preload("res://UI/hazard_alert/hazard_alert.tscn").instance()

var sequence_collection_manager : SequenceCollectionManager = load("res://Scripts/Score/SequenceCollectionManager.gd").new()
var players_pollen_score_manager
var stress_button = load("res://UI/stress_button/stress_button.tscn").instance()

const LEVEL_DURATION = 40
var timer = load("res://UI/counter/Timer.tscn").instance()
var waves_generator
var flowers_generator

func _init():
	level_bg = "park"
	level_condition = str(tr("condition_level3_part1"), LEVEL_DURATION, tr("condition_level3_part2"))
	timer.connect("time_is_up", self, "finish")
	waves_generator = level_resources.get_waves_generator()


func _ready():	
	call_deferred("add_child", waves_generator)
	call_deferred("add_child", timer)
	flowers_generator = level_resources.get_flowers_generator(200,390,490)
	call_deferred("add_child", flowers_generator)

	waves_generator.set_cooldown(8)
	waves_generator.set_can_repeat(false)
	
	alert_waves.show_alert()
	add_child(alert_waves)
	stress_button.set_lvl_bool(false)
	stress_button._ready()
	add_child(stress_button)
	
	pollen_score_view = level_resources.get_pollen_score()
	check_player_score_manager()
	players_pollen_score_manager = player.pollen_score_manager

	stress_bar_view = level_resources.get_stress_bar()
	check_player_stress_bar_manager()

	sequence_collection_manager.init(GlobalVariables.POLLEN_AVAILABLE_COLORS, players_pollen_score_manager)
	add_child(sequence_collection_manager)
	player.set_pollen_score_manager(sequence_collection_manager, true)


func start():
	timer.start(LEVEL_DURATION)
	waves_generator.start()
	.start()


func finish():
	player.set_pollen_score_manager(players_pollen_score_manager, true)
	waves_generator.stop()
	.remove_node(stress_bar_view)
	.remove_node(pollen_score_view)
	sequence_collection_manager.release()
	.remove_node(flowers_generator)
	.remove_node(stress_button)
	.remove_node(timer)
	.finish()
