extends Node2D
class_name Level

var level_bg
var level_condition
var player
var level_resources: LevelResources

var pollen_score_view
var stress_bar_view

var end_level 

func _init():
	player = GlobalVariables.get_player()
	player.position.x = 0

	level_resources = LevelResources.new()
	end_level = load("res://Scenes/EndGame.tscn")


# starts the game
func start() -> void:
	Game_Events.emit_signal("level_started", level_bg)


func finish() -> void: 
	show_end_level_completed()


func get_message() -> String:
	return level_condition


func game_over():
	# warning-ignore:return_value_discarded
	GlobalVariables.scene_switcher.change_scene("res://Scenes/game_over/GameOver.tscn")


func check_player_stress_bar_manager():
	if player.stress_manager == null:
		var manager = level_resources.get_stress_bar_manager()
		player.stress_manager = manager
		player.add_child(manager)
	player.is_on_waves_field = true
	player.stress_manager.manage_view(stress_bar_view)


func check_player_score_manager():
	if player.pollen_score_manager == null:
		var manager = level_resources.get_pollen_score_manager()
		player.set_pollen_score_manager(manager)
		player.add_child(manager)

	player.pollen_score_manager.manage_view(pollen_score_view)


func show_end_level_completed():
	var end_message = end_level.instance()
	end_message.position.x = player.global_position.x
	add_child(end_message)

	var limit_start =  end_message.position.x
	var new_rigth_limit = OS.get_window_size().x + limit_start
	player.limit_space(limit_start, new_rigth_limit)


func remove_node(node):
	if is_instance_valid(node):
		node.queue_free()
	else:
		push_error("Attempted method call on a deleted object.")
		print_debug()