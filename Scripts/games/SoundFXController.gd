extends Node2D


func _ready():
	# warning-ignore:return_value_discarded
	UI_Events.connect("sfx_music_off", self, "_on_menu_paused_efects")


func _on_menu_paused_efects():
	if $hit.volume_db > 0:
		$hit.volume_db = -80
		$get_flower.volume_db = -80
		$bee.volume_db = -80
	else :
		$hit.volume_db = 24
		$get_flower.volume_db = 2
		$bee.volume_db = -20.222
		
