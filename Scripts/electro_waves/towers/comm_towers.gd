extends Node2D
class_name CommTowers

const DISTANCE_Y = 500 

const LIMIT_EXTRA = 70
const TIME_LIMIT = 40
const TIME_LIMIT_TO_CHANGE = 10
const TIME_LIMIT_SPAWN = 2

signal finished

const LEVELS = [
	preload("res://Scripts/electro_waves/towers/first_tower_level.gd"),
	preload("res://Scripts/electro_waves/towers/second_tower_level.gd"),
	preload("res://Scripts/electro_waves/towers/third_tower_level.gd"),
	preload("res://Scripts/electro_waves/towers/fourth_tower_level.gd"),
]

var projectil_generator : Generator

var timer 
var timer_effects
var timer_close

var current_level = 1

var is_init = false
var limit_tower_A
var limit_tower_B
var bee

func _ready():
	bee = GlobalVariables.get_player()
	$AnimationPlayer.play("Idle")

func get_available_positions() -> Array:
	var position_A = $ATower.global_position
	var position_B = $BTower.global_position
	position_A.y -= DISTANCE_Y
	position_B.y -= DISTANCE_Y
	return [position_A, position_B]
	
func arise() -> void:
	$AnimationPlayer.play("raise")

func lower() -> void:
	$AnimationPlayer.play("lower")

func set_generator(generator):
	projectil_generator = generator

func start():
	arise()
	projectil_generator.stop()
	projectil_generator.set_tower_position(get_available_positions())
	
	timer_effects = ItemsGenerator.create_timer(TIME_LIMIT_TO_CHANGE)
	timer_close = ItemsGenerator.create_timer(TIME_LIMIT)
	timer = ItemsGenerator.create_timer(TIME_LIMIT_SPAWN)
	
	timer.connect("timeout", self, "spawn")
	timer_effects.connect("timeout", self, "change_level")
	timer_close.connect("timeout", self, "close")
	
	timer_close.start()
	timer_effects.start()
	timer.start()
	

func change_level():
	current_level += 1
	if current_level > LEVELS.size() :
		current_level = 1

func spawn():
	var level = LEVELS[current_level -1].new()
	level.set_wave_generator(projectil_generator)
	level.run()

func close():
	timer.stop()
	timer_effects.stop()
	timer_close.stop()
	lower()
	bee.normal_scene()
	emit_signal("finished")


func _on_LimitA_body_entered(body:Node):
	if body == bee:
		start()
		bee.static_scene(limit_tower_A, limit_tower_B)
		$Init.queue_free()


func set_position(new_position):
	limit_tower_A = $LimitA.position.x + LIMIT_EXTRA
	limit_tower_B = $LimitB.position.x - LIMIT_EXTRA

	position.x = new_position
	limit_tower_A += new_position
	limit_tower_B += new_position
