extends "res://Scripts/electro_waves/towers/level_controller.gd"

func run():
    if get_random_result():
        .generate_double_waves_d(Vector2.UP)
    else: 
        .generate_double_waves_d(Vector2.DOWN)
