extends Node2D
class_name LevelController

var generator : Generator

func set_wave_generator(projectile_generator):
    generator = projectile_generator

func generate_double_waves_v():
	generator.generate_double_waves(Vector2.UP, Vector2.DOWN, random_script())

func generate_double_waves_h():
	generator.generate_double_waves(Vector2.LEFT, Vector2.RIGHT, random_script())

func generate_double_waves_d(vector:Vector2): #Diagonal
	generator.generate_double_waves(vector + Vector2.LEFT, vector + Vector2.RIGHT, random_script())

func single_wave():
    generator.add_wave_tower()

func random_script():
    return generator.get_random_script_projectile()

func get_random_result():
    return randi() % 2 == 0;

func run():
    pass