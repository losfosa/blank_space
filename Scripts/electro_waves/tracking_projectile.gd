extends "res://Scripts/electro_waves/projectile.gd"
class_name TrackProjectile

const TARGET_ACQUIREMENT_MAX_USES = 2
var acquirement_uses_count = TARGET_ACQUIREMENT_MAX_USES

# Timer
var DEFAULT_TARGET_CORRECTION_WAIT_TIME: int = 2
var timer: Timer


func _init():
	projectile_damage = 4
	speed_increase = 40
	timer = Timer.new()
	timer.autostart = true
	timer.wait_time = DEFAULT_TARGET_CORRECTION_WAIT_TIME
	# warning-ignore:return_value_discarded
	timer.connect("timeout", self, "_timeout")


func _ready():
	$Sprite.set_animation("Tracking")
	add_child(timer)
	timer.start()


func acquire_launch_position():
	var target_separation = 900

	var left_point := Vector2(target.position.x - target_separation, target.position.y)
	var right_point := Vector2(target.position.x + target_separation, target.position.y)

	var generation_corners = [left_point, right_point]
	position = generation_corners[randi() % generation_corners.size()]


func _timeout():
	# Cash a target acquirement use
	if acquirement_uses_count == 0:
		timer.autostart = false
		timer.stop()

	acquire_target()
	acquirement_uses_count -= 1
