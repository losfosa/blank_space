extends Node2D
class_name Generator

# Projecile ratio 1:3
# For every three normal projectile comes a tracker one.

# resources
var tracking_projectile_script = load("res://Scripts/electro_waves/tracking_projectile.gd")
var normal_projectile_script = preload("res://Scripts/electro_waves/normal_projectile.gd")
var Projectile = preload("res://Scripts/electro_waves/electro_waves.tscn")
const DEFAULT_COOLDOWN = 5

var timer
var wave_target
var tower_position

var normal_projectile_count = 0
var generation_repetitions = 1
var can_repeat := true
const MAX_REPETITIONS = 5
var cooldown

const NORMAL_PROJECTILES_BATCH = 4
const TRACKING_PROJECTILES_BATCH = 2
const SCALE_TOWER_WAVE = Vector2(0.7, 0.7)


func _init():
	timer = Timer.new()
	timer.autostart = true
	timer.wait_time = DEFAULT_COOLDOWN
	timer.connect("timeout", self, "spawn_wave")

func _ready():
	wave_target = GlobalVariables.player
	add_child(timer)
	timer.start()	


func start():
	if is_inside_tree():
		timer.start()
		spawn_wave()
	else:
		push_error("Could not start waves generator")


func set_cooldown(wait_time):
	timer.wait_time = wait_time


func stop():
	get_tree().call_group("electro_waves", "queue_free")
	timer.stop()


func spawn_wave():
	if can_repeat:
		for _iteration in range(generation_repetitions):
			add_wave()
		update_generation_loop()
	else:
		add_wave()


func add_wave():
	for _generate in range(NORMAL_PROJECTILES_BATCH):
		var projectile := create_wave(get_random_script_projectile())
		projectile.set_target(wave_target)


func add_wave_tower():
	var script = get_random_script_projectile();
	create_tower_wave(script, tower_position[0])
	create_tower_wave(script, tower_position[1])


func create_tower_wave(script, default_position):
	var projectile = create_wave(script)
	projectile.scale = SCALE_TOWER_WAVE
	projectile._set_target(wave_target, default_position)


func create_wave(script) -> Projectile:
	var projectile = Projectile.instance()
	projectile.set_script(script)
	add_child(projectile)
	return projectile


func update_generation_loop():
	if generation_repetitions < MAX_REPETITIONS:
		generation_repetitions += 1
	else:
		generation_repetitions = 1
	

func get_random_script_projectile() -> Projectile:
	randomize()
	var scripts = [tracking_projectile_script, normal_projectile_script]
	return scripts[randi() % 2]


func reset():
	generation_repetitions = 1


func set_can_repeat(value):
	can_repeat = value


func set_tower_position(position_tower):
	tower_position = position_tower


func generate_double_waves(vector_a, vector_b, script):
	for position in tower_position:
		create_double_waves(vector_a, vector_b, position, script)


func create_double_waves(vector_a, vector_b, position, script):
	create_sigle_wave(get_direction(vector_a, position), position, script)
	create_sigle_wave(get_direction(vector_b, position), position, script)


func create_sigle_wave(direction, position, script):
	var projectile = create_wave(script)
	projectile.scale = SCALE_TOWER_WAVE
	projectile.set_direction(wave_target, direction, position)


func get_direction(vector, position):
	return position + (vector * 2)
