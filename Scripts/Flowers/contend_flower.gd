extends Node2D
var pos_x
var Flower = preload("res://UI/pollen/Flower.tscn")
var scene_is_permit = false
var more_distance_x = 200
var y1 = 510 
var y2 = 625

func _ready():
	pos_x = -200
	generate_pickups()


func generate_pickups():
	pos_x += more_distance_x
	$Area2D.position =  Vector2(pos_x - 500 ,rand_range(y1,y2))
	var flower = Flower.instance()
	flower.add_to_group("flowers")
	flower.global_position = Vector2(pos_x ,rand_range(y1,y2))
	call_deferred("add_child", flower)
		

func _on_Area2D_body_entered(body: Node):
	if (body == GlobalVariables.get_player()):
		generate_pickups()

func set_distance_between_flowers(quatity):
	more_distance_x = quatity
	

func set_range_in_x(y_range1 , y_range2):
	y1 = y_range1
	y2 = y_range2
	
