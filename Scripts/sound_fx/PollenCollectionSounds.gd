extends Node2D
class_name PollenCollectionSounds

var sounds = [load("res://Assets/audio/pollen_recollection/pollen_recollection_1.ogg"),
			  load("res://Assets/audio/pollen_recollection/pollen_recollection_2.ogg"),
			  load("res://Assets/audio/pollen_recollection/pollen_recollection_3.ogg")]

var failed_sequence_sound = load("res://Assets/audio/pollen_recollection/wrong_pollen_recollection.ogg")

var play_count = 0

func _init():
	var sound_paths = sounds

	for index in (sounds.size()):
		sounds[index] = SingletonSound.make_player_sound_fx(sound_paths[index])

	failed_sequence_sound = SingletonSound.make_player_sound_fx(failed_sequence_sound)


func ready() -> Node2D:
	var sound_fx_node = Node2D.new()
	for sound in sounds:
		sound_fx_node.add_child(sound)
	sound_fx_node.add_child(failed_sequence_sound)
	return sound_fx_node


func play_next():
	if not play_count < sounds.size():
		play_count = 0

	sounds[play_count].play()
	play_count += 1


func failed_sequence():
	failed_sequence_sound.play()  
