extends CanvasLayer

var is_paused = false setget set_is_paused

func _init():
	visible = false


func _ready():
	# warning-ignore:return_value_discarded
	UI_Events.connect("game_paused", self, "set_is_paused")
	$Control/EfectsOff.pressed = GlobalVariables.is_sfx_muted
	change_language()


func set_is_paused(value):
	is_paused = value
	visible = is_paused
	GameControl.pause_game()


func _on_TouchScreenButton_pressed():
	self.is_paused = !is_paused


func _on_Resume_pressed():
	self.is_paused = false
	GameControl.resume_game()
	GlobalVariables.user_interface.show()
	


func _on_TryAgain_pressed():
	GameControl.quit_game()


func _on_MuteSong_pressed():
	UI_Events.emit_signal("sfx_music_off")
	GlobalVariables.is_sfx_muted = not GlobalVariables.is_sfx_muted


func change_language():
	$ColorRect/Title.text = tr("pause_title")
	$Resume.text = tr("pause_resume_game")
	$Quit.text = tr("pause_quit")
	$Control/MuteMusic/Backgraund.text = tr("pause_background_label")
	$Control/EfectsOff/SoundEfects.text = tr("pause_sound_efect")
