extends TextureButton

func _ready():
	pressed = GlobalVariables.is_bg_muted


func _on_MuteMusic_pressed():
	UI_Events.emit_signal("bg_music_off")
	GlobalVariables.is_bg_muted = not GlobalVariables.is_bg_muted
