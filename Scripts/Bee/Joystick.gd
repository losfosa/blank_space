tool
extends TextureRect
 
signal Joystick_Start;
signal Joystick_End;
signal Joystick_Updated;
 
export (float) var radius = 30.0;
export (bool) var use_screen_rectangle = false;
export (Rect2) var screen_rectangle = Rect2();
export (Color) var editor_color = Color(1, 0, 0, 1);
 
var joystick_vector = Vector2();
var joystick_touch_id = null;
var joystick_active = false;
var joystick_ring;

export var joystick_ring_path : NodePath
 
func _ready():
	if (Engine.editor_hint == false):
		joystick_ring = get_node(joystick_ring_path);
		joystick_ring.rect_global_position = get_center_of_joystick() + rect_global_position - (joystick_ring.rect_size/2);
		joystick_vector = Vector2(0, 0);
		GameControl.connect("tree_game_paused", self, "reset_joystick")
		if (use_screen_rectangle == true):
			self.visible = true;
		else:
			self.visible = true;

func get_center_of_joystick():
	return (get_rect().position + get_rect().size/2) - rect_global_position;


func get_new_center_of_joystick():
	return get_center_of_joystick() + rect_global_position - (joystick_ring.rect_size/2);
 
 
func _input(event):
	if event is InputEventScreenTouch or event is InputEventMouseButton:
		if_event_is_Input_touch(event)	
	
	if event is InputEventScreenDrag or event is InputEventMouseMotion:
		if_event_is_input_drag(event)


func if_event_is_Input_touch(event):
	var event_is_press = true;
	if event is InputEventScreenTouch:
		event_is_press = event.pressed;
	elif event is InputEventMouseButton:
		event_is_press = event.pressed;
			
	if (event_is_press == true):
		_event_touch_is_pressed(event)	
	else:
		_event_touch_is_not_pressed(event)


func _event_touch_is_pressed(event):
	if (joystick_active == false && use_screen_rectangle == true):
		var event_position = Vector2();
		var event_ID = null;

		if event is InputEventScreenTouch:
			event_position = event.position;
			event_ID = event.index;
			
		elif event is InputEventMouseButton:
			event_position = get_global_mouse_position();
			event_ID = null;					
		_signal_screen_rectangle_has_event(event_position,event_ID)


func _signal_screen_rectangle_has_event(event_position, event_ID):
	if (screen_rectangle.has_point(event_position)):			
			rect_global_position = event_position - (rect_size/2);	
			joystick_touch_id = event_ID;		
			joystick_active = true;
			visible = true;		
			joystick_ring.rect_global_position = get_new_center_of_joystick()
			joystick_vector = Vector2(0,0);
			emit_signal("Joystick_Start");


func _event_touch_is_not_pressed(event):
	if (joystick_active == true):	
		var event_ID = null;	
		
		if event is InputEventScreenTouch:
			event_ID = event.index;
			
		elif event is InputEventMouseButton:
			event_ID = null;
		_joystick_is_not_touch_signal(event_ID)


func _joystick_is_not_touch_signal(event_ID):
	if (joystick_touch_id == event_ID):	
		self.rect_global_position = Vector2(70,400)	
		joystick_ring.rect_global_position = get_new_center_of_joystick()
		joystick_vector = Vector2(0, 0);	
		joystick_touch_id = null;
		joystick_active = false;
		
		if (use_screen_rectangle == true):
			visible = false;
		emit_signal("Joystick_End");
		self.visible =true


func if_event_is_input_drag(event):
	if (joystick_active == true):
		var event_ID = null;
		var event_position = Vector2();
			
		if event is InputEventScreenDrag:
			event_ID = event.index;
			event_position = event.position;
		elif event is InputEventMouseMotion:
			event_ID = null;
			event_position = get_global_mouse_position();
			
		if (event_ID == joystick_touch_id):
			_joystick_is_in_center(event_position)


func _joystick_is_in_center(event_position):
	if (((get_center_of_joystick() + rect_global_position) - event_position).length() <= radius):
		joystick_ring.rect_global_position = event_position - (joystick_ring.rect_size/2)
		joystick_vector = ((get_center_of_joystick() + rect_global_position) - event_position) / radius;
		emit_signal("Joystick_Updated")
	else:
		_joystick_isnt_in_center(event_position)		


func _joystick_isnt_in_center(event_position):
	joystick_vector = ((get_center_of_joystick() + rect_global_position) - event_position).normalized()
	joystick_ring.rect_global_position = get_new_center_of_joystick()
	joystick_ring.rect_global_position -= joystick_vector * radius;
	emit_signal("Joystick_Updated");


func reset_joystick():
	joystick_touch_id = null
	_joystick_is_not_touch_signal(null)
	
