extends CanvasLayer

var max_value
var aux_position

func init(limit = 100):
	$Bar.max_value = 100
	max_value = limit
	aux_position = $Sprite.position.x


func update_bar(new_value):
	var bar_progress = (new_value/max_value) * 100
	$Bar.value = bar_progress
	$Sprite.position.x = bar_progress * ($Bar.rect_size.x / 100) + aux_position
