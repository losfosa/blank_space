extends CanvasLayer
class_name CounterGroup

var icon_path = load("res://UI/counter/PollenItemGroupCounter.tscn")

var icons : Array
const DISTANCE = 40
var start_position : Vector2


func measure_start_position(amount_total_items : int):
	start_position.x = $Pollen.position.x - (DISTANCE * amount_total_items)
	start_position.y = $Pollen.position.y

	
func add_icon(color_name : String):
	var icon = icon_path.instance()
	icon.init(color_name, start_position)
	icons.append(icon)
	start_position.x += DISTANCE
	add_child(icon)


func activate_icon(index:int):
	icons[index].restore_opacity()


func deactivate_all():
	for icon in icons:
		icon.low_opacity()
