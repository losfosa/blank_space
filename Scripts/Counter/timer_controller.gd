extends CanvasLayer
const WAIT_TIME = 1
const TIME_UNIT = 60
const UNITS_LIMIT = 9

signal time_is_up

var timer
var curren_time 

func _ready():
	timer = ItemsGenerator.create_timer(WAIT_TIME)
	timer.connect("timeout", self, "update_timer")


func start(time):
	curren_time = time
	timer.start()
	update_timer_label()


func update_timer():
	curren_time -= WAIT_TIME
	update_timer_label()
	if curren_time == 0 :
		emit_signal("time_is_up")
		timer.stop()


func update_timer_label():
	$Container/Minutes.text = get_minutes()
	$Container/Seconds.text = get_seconds()


func get_minutes() -> String:
	return format_time(curren_time / TIME_UNIT)


func get_seconds() -> String:
	return format_time(curren_time % TIME_UNIT)


func format_time(number: int) -> String:
	return String(number) if number > UNITS_LIMIT else "0" + String(number)


func plus_extra_time(extra_time: int):
	curren_time += extra_time
