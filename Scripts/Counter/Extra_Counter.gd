extends Control
class_name ExtraCounter

var counter = 0
var constant_label
var max_value
var signal_name


func update_text(current_value_counter:= "0"):
	$Label.text = current_value_counter + constant_label


func complete():
	get_node("Label").add_color_override("font_color", Color("#b6f455"))
	get_node("Label").add_color_override("font_outline_modulate", Color("#285430"))
	mark_counter_completed(true)


func set_constant_label(_constant:String):
	constant_label = _constant


func set_max_value(new_limit):
	max_value = new_limit


func _connect_to(_signal_name: String):
	# warning-ignore:return_value_discarded
	UI_Events.connect(_signal_name, self, "count")
	signal_name = _signal_name


func count(value):
	if value < max_value:
		update_text(String(value))
	elif value >= max_value:
		update_text(String(max_value))
		complete()
		UI_Events.disconnect(signal_name, self, "count")


func mark_counter_completed(visible:bool):
	$Check.visible = visible