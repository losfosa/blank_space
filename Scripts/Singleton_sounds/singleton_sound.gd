extends Node

# Background music
var SOUND_CITY = load("res://Assets/audio/in_game_music/Chronos.ogg")
var SOUND_HIVE = load("res://Assets/audio/in_game_music/EchoesIreland.ogg")
var SOUND_PARK = load("res://Assets/audio/in_game_music/IntheMemoryFiftySounds.ogg")
var SOUND_MENU = load("res://Assets/audio/in_game_music/Eurynome.ogg")

# Sound FX
var PLAYER_GOT_HIT = load("res://Assets/audio/in_game_music/sound_effects/cable.ogg")
var POLLEN_COLLECTED = load("res://Assets/audio/in_game_music/sound_effects/mixkit-achievement-bell-600.ogg")

var LEVEL_FINISHED = load("res://Assets/audio/in_game_music/sound_effects/ta-da-winner.ogg")

const DEFAULT_BG_VOLUME := -12
const DEFAULT_FX_VOLUME := 4


func make_player_bg(file_sound, volume := DEFAULT_BG_VOLUME) -> AudioStreamPlayer:
	var player = Sound.new()
	player.init(file_sound, "bg_music_off", volume, true)

	if GlobalVariables.is_bg_muted:
		player.mute()
	return player
	
	
func make_player_sound_fx(file_sound, volume := DEFAULT_FX_VOLUME) -> AudioStreamPlayer:
	var player = Sound.new()
	player.init(file_sound, "sfx_music_off", volume)
	
	if GlobalVariables.is_sfx_muted:
		player.mute()
	return player
