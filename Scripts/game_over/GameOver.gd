extends CanvasLayer

var music_player


func _init():
	var menu_music = load("res://Assets/audio/in_game_music/menus/game_over.ogg")
	music_player = SingletonSound.make_player_sound_fx(menu_music) as Sound


func _ready():
	add_child(music_player)
	music_player.play()
	change_languaje()


func _on_Try_Again_button_down():
	var game = load("res://Game/GameCore.tscn").instance()
	GlobalVariables.player.pollen_score_manager.restore()
	# warning-ignore:return_value_discarded
	GlobalVariables.scene_switcher.change_scene_using_node(game)
	music_player.stop()


func _on_Exit_button_down():
	music_player.stop()
	GameControl.quit_game()
	
func change_languaje():
	$ColorRect/Title.text = tr("game_over_title")
	$ColorRect/Label.text = tr("game_over_try_again")
	$Try_Again.text = tr("game_over_yes")
	$Exit.text = tr("game_over_exit")
