extends CanvasLayer

var notify
var is_lvl_2 = true

func _ready():
	if is_lvl_2:
		notify = preload("res://UI/stress_button/alert_explication_stress.tscn")
		var pop_up = notify.instance()
		pop_up.init(3.5)
		add_child(pop_up)
		pop_up.start(tr("stress_button"))


func _process(_delta):
	if GlobalVariables.player != null && GlobalVariables.player.reduce_permit_boolean() == false:
		$TouchScreenButton.self_modulate.a = 0.3
	else:
		$TouchScreenButton.self_modulate.a = 1


func _on_TouchScreenButton_pressed():
	GlobalVariables.player.reduce_stress_button(1,20)


func set_lvl_bool(lvl:bool):
	is_lvl_2 = lvl
